import './assets/main.css'
// docs: https://bootstrap-vue-next.github.io/bootstrap-vue-next/docs/components.html
// https://datatables.net/blog/2022-06-22-vue
import BootstrapVueNext from 'bootstrap-vue-next'
import 'bootstrap-vue-next/dist/bootstrap-vue-next.css'
import "bootstrap/dist/css/bootstrap.min.css"
//import "bootstrap"


import { createApp } from 'vue'
import App from './App.vue'
import router from './router'


const app = createApp(App).use(router)
app.use(BootstrapVueNext)
app.mount('#app')
